import java.util.ArrayList;

import javax.swing.JOptionPane;

public class Problem1 {

	public static void main(String[] args) {
		//Algorithm to solve the problem
		//1) create a popup box to get the number of girl scouts which can be used to create the array
		//2) create the array
		//3) use a while loop to go through the array and find out how much goes into each array slot
		//4) divide the array slots into categories
		//5) print out the results
		 
		
		// TODO Auto-generated method stub

		//prompt for the number of girl scouts
		int size = Integer.parseInt(JOptionPane
				.showInputDialog("Enter the number of girl scouts"));
		int[] girlScout = new int[size];

		int i = 0, lessThan10=0, lessThan20=0, lessThan30=0, lessThan40=0, moreThan40=0;
		
		while (i < size) {
			//prompt for the number of cookie box sold for each girl scout
			//categories the quantities and counts how many there are of each
			girlScout[i] = Integer.parseInt(JOptionPane
					.showInputDialog("Enter the number of cookie boxes for girl scout #"
							+ (i+1)));
			if(girlScout[i] <= 10)
			{
				lessThan10 = lessThan10 + 1;
			}
			if(girlScout[i] <= 20 && girlScout[i] > 10)
			{
				lessThan20 = lessThan20 + 1; 
			}
			if(girlScout[i] <= 30 && girlScout[i] > 20)
			{
				lessThan30 = lessThan30 + 1; 
			}
			if(girlScout[i] <= 40 && girlScout[i] > 30)
			{
				lessThan40 = lessThan40 + 1; 
			}
			if(girlScout[i] > 40)
			{
				moreThan40 = moreThan40 + 1;
			}
			
			
			i++;
		}
		//Prints out the results of how many boxes and how many girl scouts sold that many boxes
		
		System.out.println("Total Boxes        |     Number Of Girl Scouts");
		System.out.println(" 0 to 10           |              "+ lessThan10   );
		System.out.println("11 to 20	   |   		  "+ lessThan20   );
		System.out.println("21 to 30	   |		  "+ lessThan30   );
		System.out.println("31 to 40	   |		  "+ lessThan40   );
		System.out.println("41 or more	   |		  "+ moreThan40   );
	}
}